from . import core
from . import settings

trusted = settings.adminTrusted

def kick(client, reason):
    core.irc.send(("KICK ##thunix "+client+" :"+reason+"\n").encode())
