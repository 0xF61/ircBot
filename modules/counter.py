from datetime import datetime
from . import core
from . import settings
class Counter():
    def __init__(self):
        self.counter = 0
        self.flag = 0
        self.channel = ""
        self.timeout = datetime.today()
    def resetFlag(self):
        self.counter = 0
        self.flag = 0
    def redFlag(self):
        self.flag = 1
        self.timeout = datetime.today()
    def counterPlus(self):
        self.counter = self.counter + 1
        self.timeout = datetime.today()
        if self.counter == settings.counterIter:
            self.redFlag()
            core.irc.send(bytes("PRIVMSG "+self.channel+" :Calm down.\n", "utf-8"))
    def checkIfReset(self, time):
        if core.dateDiffInSeconds(time, datetime.today()) >= settings.counterDelay:
            self.resetFlag()
