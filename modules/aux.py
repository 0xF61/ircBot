from . import core
from . import settings
import socket
import base64

def uptime():
    return core.run("uptime")

def users():
    cmd = "user=$(users | tr ' ' '\n' | sort -u | tr '\n' ' '); online=$(users | tr ' ' '\n' | sort -u | wc -l);offline=$( grep -c ':/home/' /etc/passwd );echo \"Online User $online/$offline --> $user\""
    return core.run(cmd)

def allusers():
    with open('/etc/passwd', 'r') as passwd:
        return passwd.read()

def sensor():
    cmd = "sensors | grep Core  | sed 's/(.*//g' | sed 's/.*+//g' | tr '\n' ' '"
    return core.run(cmd)

def freeRam():
    cmd ="free -h | grep Mem | awk '{print $3\" out of \" $2 \" used, approx. \"$7\" available.\"}'"
    return core.run(cmd)

def freeHDD():
    cmd ="df -h | grep home | awk '{print $3\" out of \" $2 \" used, approx. \" $4\" available.\"}'"
    return core.run(cmd)

def freePort():
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.bind(('', 0))
    addr, port = tcp.getsockname()
    tcp.close()
    return str(port)

def encodeBase64(string):
    return base64.b64encode((string).encode()).decode()

def decodeBase64(string):
    try:
        return base64.b64decode((string).encode()).decode()
    except:
        return "This is not a valid base64 string."

def motivate():
    cmd = "curl -s https://thunix.org/~krystianbajno/public/fortune.php | sed 's#&[^;]*;##g' | tr '\n\t\t' ' '"
    return core.run(cmd)


