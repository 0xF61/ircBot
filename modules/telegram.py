#!/usr/bin/python3
import json
import requests
from time import sleep as sleep

creds = open("creds").readline()
TOKEN = str(creds).strip()

BASE="https://api.telegram.org/"

class Telegram:
    def __init__(self, token):
        self.token = token
        self.TELEGRAM    = requests.get(BASE+"bot"+self.token+"/getUpdates").json()
        self.lastReplied = self.resultSize = len(self.TELEGRAM["result"])

        self.waitSec = 5
        while self.resultSize <= self.lastReplied:
            print("No message Found. Waiting "+str(self.waitSec)+" second.")
            sleep(self.waitSec)
            self.TELEGRAM    = requests.get(BASE+"bot"+self.token+"/getUpdates").json()
            self.resultSize = len(self.TELEGRAM["result"])
        else:
            self.message = self.TELEGRAM["result"][self.lastReplied].get("message")
            if self.message != None:
                self.message_id               = self.message["message_id"]
                self.messageFrom              = self.message["from"]
                self.messageFromId            = self.messageFrom["id"]
                self.messageFromIs_bot        = self.messageFrom["is_bot"]
                self.messageFromFirst_name    = self.messageFrom["first_name"]
                self.messageFromUsername      = self.messageFrom["username"]

                self.messageChat              = self.message["chat"]
                self.messageType              = self.messageChat["type"]
                 
            self.Photo            	          = self.message.get("photo")
            if self.Photo != None:
                self.file_id                  = self.Photo[-1]["file_id"]

            self.messageDocument              = self.message.get("document")
            if self.messageDocument != None:
                self.mime_type                = self.messageDocument["mime_type"]
                self.fileName                 = self.messageDocument["file_name"]
                self.file_id                  = self.messageDocument["file_id"]
                self.file_size                = self.messageDocument["file_size"]

            self.Contact           	          = self.message.get("contact")
            if self.Contact != None:
                self.first_name               = self.Contact["first_name"]
                self.last_name                = self.Contact["last_name"]
                self.phone_number             = self.Contact["phone_number"]
                self.user_id                  = self.Contact["user_id"]

            self.date			 = self.message["date"]
            self.caption		 = self.message.get("caption")
            self.text			 = self.message.get("text")

    def sendMessage(self,chatid,text):
      requests.get(BASE+"bot"+TOKEN+"/sendMessage?chat_id="+str(chatid)+"&text="+text+"")

    def sendMessage(chatid,text):
      requests.get(BASE+"bot"+TOKEN+"/sendMessage?chat_id="+str(chatid)+"&text="+text+"")

    def getFilePath(self, file_id):
      file_path = requests.get(BASE+"bot"+TOKEN+"/getFile?file_id="+file_id).json()
      file_path = file_path["result"]["file_path"]
      return file_path

    def download(self, file_path):
      r = requests.get(BASE+"file/bot"+TOKEN+"/"+file_path, stream=True)
      with open(file_path, 'wb') as f:
          for chunk in r.iter_content(chunk_size=1024):
              if chunk:
                  f.write(chunk)

    def wait(self, time):
        sleep(time)

    def update(self, lastReplied):
        self.TELEGRAM    = requests.get(BASE+"bot"+self.token+"/getUpdates").json()
        self.lastReplied =  lastReplied
        self.resultSize = len(self.TELEGRAM["result"])

        self.waitSec = 5
        while self.resultSize == self.lastReplied:
            print("No message Found. Waiting "+str(self.waitSec)+" second.")
            sleep(self.waitSec)
            self.TELEGRAM    = requests.get(BASE+"bot"+self.token+"/getUpdates").json()
            self.resultSize = len(self.TELEGRAM["result"])
        else:
            self.message = self.TELEGRAM["result"][self.lastReplied].get("message")
            if self.message != None:
                self.message_id               = self.message["message_id"]
                self.messageFrom              = self.message["from"]
                self.messageFromId            = self.messageFrom["id"]
                self.messageFromIs_bot        = self.messageFrom["is_bot"]
                self.messageFromFirst_name    = self.messageFrom["first_name"]
                self.messageFromUsername      = self.messageFrom["username"]

                self.messageChat              = self.message["chat"]
                self.messageType              = self.messageChat["type"]


                 
            self.Photo            	          = self.message.get("photo")
            if self.Photo != None:
                self.file_id                  = self.Photo[-1]["file_id"]

            self.messageDocument              = self.message.get("document")
            if self.messageDocument != None:
                self.mime_type                = self.messageDocument["mime_type"]
                self.fileName                 = self.messageDocument["file_name"]
                self.file_id                  = self.messageDocument["file_id"]
                self.file_size                = self.messageDocument["file_size"]

            self.Contact           	          = self.message.get("contact")
            if self.Contact != None:
                self.first_name               = self.Contact["first_name"]
                self.last_name                = self.Contact["last_name"]
                self.phone_number             = self.Contact["phone_number"]
                self.user_id                  = self.Contact["user_id"]

            self.date			 = self.message["date"]
            self.caption			 = self.message.get("caption")
            self.text			 = self.message.get("text")

        return self.resultSize
