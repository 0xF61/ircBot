import random
from . import admin
from . import settings

class Bullet():
    def __init__(self):
        self.bullet = random.randint(1,settings.cylinderSize)
        self.lastnick = ""
    def pull(self):
        self.bullet = self.bullet - 1
        if self.bullet == 0:
            self.random()
            admin.kick(self.lastnick, "You've shot yourself in the head!")
            return "**Bang!**"
        else:
            return "**Click**"
    def random(self):
        self.bullet = random.randint(1,settings.cylinderSize)
        return "*Drum roll*"
