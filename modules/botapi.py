########################################
# API functions used for bot threading #
########################################

import re
import socket
import threading
from . import core
from . import settings
from .telegram import Telegram

if settings.delayedMessage == 1:
    from . import delayedMessage
if settings.smtp == 1:
    from . import smtp
if settings.aux == 1:
    from . import aux
if settings.admin == 1:
    from . import admin
if settings.revolver == 1:
    from . import revolver
    revolv = revolver.Bullet()

def tBOT(TOKEN, channel):
    xf61 = Telegram(TOKEN)

    while xf61.resultSize >= xf61.lastReplied:
        #if xf61.text is not None and xf61.messageType == "group":
        if xf61.text is not None:
            ret = xf61.messageFromUsername+": "+xf61.text
            core.sendMsg(0, channel, ret)
            xf61.lastReplied = xf61.lastReplied + 1
            xf61.update(xf61.lastReplied)
        else:
            continue
    xf61.lastReplied = xf61.lastReplied + 1
    xf61.update(xf61.lastReplied)

def help(nick, channel):
# Core + Utility
    mesaj = "Core: help code | Utility: "
    if settings.aux == 1:
            mesaj += "base64 df(freeHDD) fr(freeRam) fp(freePort) sensor uptime users "
    if settings.smtp == 1:
        mesaj += "request sendmail "
# Entertainment
    mesaj +="| Entertainment: "
    if settings.revolver == 1:
        mesaj+="pull roll "
    if settings.aux == 1:
        mesaj +="motivate "
    if core.checkPrivileges(nick, settings.adminTrusted) == 1:

# Admin
        mesaj += "| Admin: "
        if settings.admin == 1:
            mesaj += "kick "
        if settings.delayedMessage == 1:
            mesaj += "timed-message "
# Send
    core.sendMsg(0, channel,mesaj)
    return

def code(channel):
    code = "https://github.com/mcveri/ircBot, https://github.com/krystianbajno/ircBot"
    core.sendMsg(0, channel,code)
    return
### Aux

if settings.aux == 1:

    def base64(channel, msg):
        if len(msg) > 2:
            if msg[1] == "encode":
                core.sendMsg(0, channel, aux.encodeBase64(" ".join(str(x) for x in msg[2:])))
            elif msg[1] == "decode":
                core.sendMsg(0, channel, aux.decodeBase64(" ".join(str(x) for x in msg[2:])))
            else:
                core.sendMsg(0, channel, "Usage: .b64 <decode/encode> <string>")
        else:
            core.sendMsg(0, channel, "Usage: .b64 <decode/encode> <string>")
        return

    def motivate(channel):
        core.sendMsg(0, channel,aux.motivate())
        return

    def sensor(channel):
        core.sendMsg(0, channel,aux.sensor())
        return

    def uptime(channel):
        core.sendMsg(0, channel,aux.uptime())
        return

    def users(channel):
        core.sendMsg(0, channel,aux.users())
        return

    def freeram(channel):
        core.sendMsg(0, channel,aux.freeRam())
        return

    def freehdd(channel):
        core.sendMsg(0, channel,aux.freeHDD())
        return

    def fp(channel, msg):
        tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if len(msg) > 1:
                port = ''.join(x for x in msg[1] if x.isdigit())
                if int(port) > 0 and int(port) < 65536:
                    result = tcp.connect_ex(('127.0.0.1',int(port)))
                    tcp.close()
                    if result == 0:
                        core.sendMsg(0, channel,"Port "+port+" is not available.")
                    else:
                        core.sendMsg(0, channel,"Port "+port+" is available to use.")
                else:
                    core.sendMsg(0, channel,"Port "+port+" is not between 1-65535")
        else:
            core.sendMsg(0, channel,"Port "+aux.freePort()+" is available to use.")
        return
# Admin

if settings.admin == 1:

    def kick(channel, msg, nick):
                if core.checkPrivileges(nick, settings.adminTrusted) == 1: 
                    if len(msg) > 1:
                        if len(msg) == 2:
                            admin.kick(msg[1], "You've been kicked.")
                        elif len(msg) > 2:
                         admin.kick(msg[1], (" ".join(str(x) for x in msg[2:])))
                    else:
                        core.sendMsg(1, channel,"Usage: .kick <name> <reason>")
                return

if settings.delayedMessage == 1:

    def manageTimedMessage(msg, channel, nick):
        def printHelp():
            core.sendMsg(1, channel, ".timed-message <on/off/interval/motivation (on/off)/set string (string)>")
        if core.checkPrivileges(nick, settings.adminTrusted) == 1:
            if len(msg) == 2:
                if msg[1] == "on":
                    if settings.turnOnTimedMessage == 1:
                        core.sendMsg(1, channel,"Timed message is already on.")
                    else:
                        settings.turnOnTimedMessage = 1
                        start = threading.Thread(target=delayedMessage.timedMessage, args=(settings.channel,))
                        start.start()  
                elif msg[1] == "off":
                    if settings.turnOnTimedMessage == 0:
                        core.sendMsg(1, channel, "Timed message is already off.")
                    else:
                        settings.turnOnTimedMessage = 0
                else:
                    try:
                        settings.timedMessageInterval = float(msg[1])
                        core.sendMsg(1, channel, "Timed message interval changed to "+ msg[1] + " minutes.")
                        return
                    except:
                        printHelp()
                        return
            elif len(msg) == 3 and settings.aux == 1:
                if msg[1] == "motivation":
                    if msg[2] == "on":
                        if settings.timedMessageMotivation == 1:
                            core.sendMsg(1, channel, "Motivation is already on.")
                        else:
                            settings.timedMessageMotivation = 1
                            core.sendMsg(1, channel, "Motivation on.")
                            return
                    if msg[2] == "off":
                        if settings.timedMessageMotivation == 0:
                            core.sendMsg(1, channel, "Motivation is already off.")
                        else:
                            settings.timedMessageMotivation = 0
                            core.sendMsg(1, channel, "Motivation off.")
                    else:
                        printHelp()
            elif len(msg) > 3:
                if msg[1] == "set" and msg[2] == "string":
                    settings.timedMessageString = (" ".join(str(x) for x in msg[3:]))
                    core.sendMsg(1, channel, "String set to: '"+settings.timedMessageString+"'")
                else:
                    printHelp()
            else:
                printHelp()
        else:
            return

# Mailing and messaging

if settings.smtp == 1:
    def request(channel, nick, msg):
            if len(msg) == 3:
                regex = re.compile(r''+msg[1]+'')
                if regex.search(aux.allusers()):
                    core.sendMsg(0, channel,"Request not sent. - User exists. Try another username.")
                else:
                    if channel == "##thunix":
                        smtp.request(nick,msg[1], msg[2])
                    else:
                        smtp.request(channel,msg[1], msg[2])
                    core.sendMsg(0, channel,"Request sent.")
            else:
                core.sendMsg(0, channel,"Provide proper username and email! - .request <username> <email>")
            return

    def sendmail(channel, msg, nick):
            if core.checkPrivileges(nick, smtp.trusted) == 1:
                if len(msg) >= 6:
                    try:
                        i = int(msg[3])
                    except:
                        core.sendMsg(0, channel,smtp.privileged_string)
                        return
                    subject = (" ".join(str(x) for x in msg[4:4+i]))
                    message = (" ".join(str(x) for x in msg[4+i:])).replace("\\r\\n", "\r\n")
                    try:
                        smtp.sendmail(msg[1], msg[2], subject, message)
                        core.sendMsg(0, channel,"Message sent.")
                        return
                    except:
                        core.sendMsg(0, channel,smtp.privileged_string)
                        return
                else:
                    core.sendMsg(0, channel, smtp.privileged_string)
            else:
                if len(msg) >= 5:
                    try:
                        i = int(msg[2])
                    except:
                        core.sendMsg(0, channel,smtp.unprivileged_string)
                    subject = (" ".join(str(x) for x in msg[3:3+i]))
                    message = (" ".join(str(x) for x in msg[3+i:])).replace("\\r\\n", "\r\n")
                    try:
                        smtp.sendmail(nick+"@thunix.org", msg[1], subject, message)
                        core.sendMsg(0, channel,"Message sent.")
                    except:
                        core.sendMsg(0, channel,smtp.unprivileged_string)
                else:
                    core.sendMsg(0, channel, smtp.unprivileged_string)
            return

    def report(channel, nick, msg):
            if len(msg) >= 3:
                msg.pop(0)
                email=msg.pop(0)
                regex = re.compile(r''+email+'')
                if channel == "##thunix":
                    smtp.report(nick,email, ' '.join(msg))
                else:
                    smtp.report(channel,email, ' '.join(msg))
                core.sendMsg(0, channel,"Report sent.")
            else:
                core.sendMsg(0, channel,"Provide proper email and description! - .report <email> <description of issue>")
            return

# Entertainment

if settings.revolver == 1:

    def pull(channel, nick):
            core.sendMsg(0, channel, revolv.pull())
            revolv.lastnick = nick
            return

    def roll(channel, msg):
        if len(msg) > 1:
            try:
                settings.cylinderSize = int(msg[1])
                if int(msg[1]) > 0 and int(msg[1]) < 21:
                    core.sendMsg(1, channel, "** You picked the revolver with cylinder size of " + str(int(msg[1])) + ". **")
                    core.sendMsg(0, channel, revolv.random())
                    return
                else:
                    core.sendMsg(1, channel, "Cylinder size must be between 1-20. Did you know that Le Faucheux 20 revolver had a cylinder size of 20?")
                    return
            except:
                core.sendMsg(0, channel, "Provide proper revolver cylinder size.")
                return
        else:
            core.sendMsg(0, channel, revolv.random())
            return 
