from . import core
import ssl
import socket
from email import utils
from . import admin
from . import settings
from . import aux

smtpLogin = settings.smtpLogin
smtpPassword = settings.smtpPassword
trusted = settings.smtpTrusted
privileged_string = settings.smtpPrivilegedString
unprivileged_string = settings.smtpUnprivilegedString

class Smtp():
    def __init__(self):
        self.login = smtpLogin
        self.password = smtpPassword
        self.smtp = ssl.wrap_socket(socket.socket(socket.AF_INET, socket.SOCK_STREAM),ssl_version=ssl.PROTOCOL_SSLv23)
        self.base64_str = aux.encodeBase64(("\x00"+self.login+"\x00"+self.password)).encode()
    def authenticate(self):
        self.smtp.connect(('mail.thunix.org', 465))
        self.smtp.send('EHLO Requester\r\n'.encode()+"AUTH PLAIN ".encode()+self.base64_str+"\r\n".encode())
    def sendMsg(self, mailfrom, mailto, subject, msg):
        self.smtp.send(("MAIL FROM: "+mailfrom+"\r\nRCPT TO: "+mailto+"\r\n \
            DATA\r\nDate:"+utils.formatdate()+"\r\nTo:"+mailto+"\r\nFrom: "+mailfrom+"\r\nSubject: "+ subject+"\r\n" + msg + "\r\n.\r\n QUIT\r\n" ).encode())
        for i in range(1,8):
            core.printLog(self.smtp.recv(1024).decode())
    def closeConnection(self):
        self.smtp.close()

def request(ircName, nickName, email):
   smtp = Smtp()
   smtp.authenticate()
   smtp.sendMsg("smtpsupplicant@thunix.org", "hexhaxtron@thunix.org", "[REGISTRATION] "+ircName, "Hello hexhaxtron, my desired nickname is "+nickName+".\r\nPlease respond to "+email+".\r\nThis message was generated automatically by thunix bot.")
   smtp.closeConnection()

def report(ircName, email, issue):
   smtp = Smtp()
   smtp.authenticate()
   smtp.sendMsg("smtpsupplicant@thunix.org", "hexhaxtron@thunix.org", "[ISSUE] "+ircName, "Hello hexhaxtron, "+ircName+" <"+email+"> reports an issue:\r\n"+issue+"\r\n\r\nThis message was generated automatically by thunix bot.")
   smtp.closeConnection()

def sendmail(mailfrom, mailto, subject, message):
    smtp = Smtp()
    smtp.authenticate()
    smtp.sendMsg(mailfrom, mailto, subject, message)
    smtp.closeConnection()
