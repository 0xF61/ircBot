from . import core as core
from . import settings as settings
from . import aux as aux
from time import sleep
from datetime import datetime

def timedMessage(channel):
    time = datetime.today()
    core.sendMsg(1, channel, "Timed message turned on.")
    while settings.turnOnTimedMessage == 1:
        if core.dateDiffInSeconds(time, datetime.today()) >= settings.timedMessageInterval*60:
            time = datetime.today()
            if settings.timedMessageMotivation == 1:
                core.sendMsg(1, channel, aux.motivate())
            core.sendMsg(1, channel, settings.timedMessageString)
        sleep(1)
    core.sendMsg(1, channel, "Timed message turned off.")
    return
