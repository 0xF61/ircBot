import socket
import subprocess
import sys
import random
from . import counter as counter
from . import settings

irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
count = counter.Counter()

### Base Func -- Start

def printLog(string):
    with open("ircchat.log", "a") as log:
        log.write(string+"\r\n")
    print(string)

def encode(string):
    string = bytes(string, "utf-8")
    return string

def decode(string):
    string = string.decode("utf-8")
    string = string.strip('\r\n')
    return string

def sendMsg(system, channel, ircMessage): # system == 0 -> send normally with counter, system == 1 -> force
    msg = "PRIVMSG {0} :{1}\n".format(channel, ircMessage)
    msg = encode(msg)
    if system != 1:
        count.checkIfReset(count.timeout)
        if count.flag == 0:
            count.counterPlus()
            irc.send(msg)
    else:
        irc.send(msg)
    return
    
def ping():
    irc.send(bytes("PONG :pings\n", "utf-8"))
    return

def getMsg():
    ircmsg = irc.recv(2048)
    ircmsg = decode(ircmsg)
    printLog(ircmsg)
    return ircmsg

# if returns 1, then user is privileged.
def checkPrivileges(nick, wlist):
    for value in wlist:
        if nick == value:
            return 1
        else: continue

def run(command):
    output = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
    output, err = output.communicate()
    output = output.decode("utf-8")
    output = output.strip("\n")
    return output

def dateDiffInSeconds(date2, date1):
    timedelta = date1 - date2
    return timedelta.seconds

### Base Func -- END
