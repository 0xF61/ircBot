#!/usr/bin/env python3

import re
import socket
import threading

import modules.core as core
import modules.botapi as api
import modules.settings as settings
from modules.telegram import Telegram as xf61

# main functions of the bot
def main():
  creds = open("creds").readline()
  TOKEN = str(creds).strip()

  ### CHANGE HERE
  join  = "##thunix"
  chatId=-187976886
  ### CHANGE HERE

  telegram = threading.Thread(target=api.tBOT, args=(TOKEN, join,))
  telegram.start()


  core.irc.connect((settings.Server, settings.Port))
  core.irc.send(settings.NICK)
  core.irc.send(settings.USER)
  core.irc.send(('JOIN '+ settings.channel +'\r\n').encode())
  count = core.count

  #open the chat log file if it exists and delete it to start fresh.
  with open("ircchat.log", "w") as tmp:
     tmp.write("")
  #Boot msg:
  channel = settings.channel
  core.sendMsg(1, channel, settings.greetMessage)
  if settings.delayedMessage == 1 and settings.turnOnTimedMessage == 1:
        api.manageTimedMessage([".timed-message", "off"], settings.channel, "krystianbajno")
        api.manageTimedMessage([".timed-message", "on"], settings.channel, "krystianbajno")

  # start infinite loop to continually check for and receive new info from server
  while 1: 
    # clear ircmsg value every time
    ircmsg = ""
    # set ircmsg to new data received from server
    ircmsg = core.getMsg().strip('\n\r')
    # repsond to pings so server doesn't think we've disconnected
    textArray = ircmsg.split(" ")
    if textArray[0] == "PING":
        core.ping()
        continue
    else:
        channel = textArray[2]
        nick    = textArray[0].split("!")
        nick    = nick[0].lstrip(":")
        m       = textArray[3:]
        msg     = ""
        for s in m:
            msg += s+" "
        msg = msg.strip().lstrip(":")

        ### If bot recieve a message on PM change channel to nick.
        ### This is way to respond PM.
        if channel == settings.botNick:
            channel = nick
        count.channel = channel

    if channel == join:
        ret = nick+": "+msg
        xf61.sendMessage(chatId,ret)
    print(channel+" "+join+" "+str(chatId))

    msg = msg.split(" ")
    msg[0] = msg[0].lower()

    if msg[0] == ".code":
        code = threading.Thread(target=api.code, args=(channel,))
        code.start()

    if msg[0] == ".help" or msg[0] == ".h":
        help = threading.Thread(target=api.help, args=(nick, channel,))
        help.start()

    if settings.smtp == 1:

        if msg[0] == ".request" or msg[0] == ".r":
            request = threading.Thread(target=api.request, args=(channel,nick,msg,))
            request.start()

        if msg[0] == ".report":
            report = threading.Thread(target=api.report, args=(channel,nick,msg,))
            report.start()

        if msg[0] == ".sendmail":
            sendmail = threading.Thread(target=api.sendmail, args=(channel,msg,nick,))
            sendmail.start()

    if settings.aux == 1:

        if msg[0] == ".base64" or msg[0] == ".b64":
            base64 = threading.Thread(target=api.base64, args=(channel,msg,))
            base64.start()

        if msg[0] == ".sensor":
            sensor = threading.Thread(target=api.sensor, args=(channel,))
            sensor.start()

        if msg[0] == ".motivate" or msg[0] == ".m":
            motivate = threading.Thread(target=api.motivate, args=(channel,))
            motivate.start()

        if msg[0] == ".uptime":
            uptime = threading.Thread(target=api.uptime, args=(channel,))
            uptime.start()
                
        if msg[0] == ".users":
            users = threading.Thread(target=api.users, args=(channel,))
            users.start()

        if msg[0] == ".fp":
            fp = threading.Thread(target=api.fp, args=(channel,msg,))
            fp.start()

        if msg[0] == ".freeram" or msg[0] == ".fr":
            freeram = threading.Thread(target=api.freeram, args=(channel,))
            freeram.start()

        if msg[0] == ".df" or msg[0] == ".freehdd" or msg[0] == ".fh":
            freehdd = threading.Thread(target=api.freehdd, args=(channel,))
            freehdd.start()

    if settings.revolver == 1:

        if msg[0] == ".pull" or msg[0] == ".p":
            pull = threading.Thread(target=api.pull, args=(channel,nick,))
            pull.start()

        if msg[0] == ".roll":
            roll = threading.Thread(target=api.roll, args=(channel,msg,))
            roll.start()

    if settings.admin == 1:

        if msg[0] == ".kick":
            kick = threading.Thread(target=api.users, args=(channel,msg,nick,))
            kick.start()

    if settings.delayedMessage == 1:    
        if msg[0] == ".timed-message":
            timedMessage = threading.Thread(target=api.manageTimedMessage, args=(msg,channel,nick, ))
            timedMessage.start()

#start main function
if __name__ == "__main__":
    main()
